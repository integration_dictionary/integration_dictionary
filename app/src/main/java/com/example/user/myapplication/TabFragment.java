package com.example.user.myapplication;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.ListFragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.LinkedList;

import static com.example.user.myapplication.R.color.md_blue_500;

public class TabFragment extends Fragment {
    public FragmentPagerAdapter adapter;
    private SlidingTabLayout tabs;
    private ViewPager pager;

    public static Fragment newInstance() {
        TabFragment f = new TabFragment();
        return f;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.frg_tab, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        //adapter
        final LinkedList<Fragment> fragments = getFragments();
        adapter = new TabFragmentPagerAdapter(getFragmentManager(), fragments);
        //pager
        pager = (ViewPager) view.findViewById(R.id.pager);
        pager.setAdapter(adapter);
        //tabs
        tabs = (SlidingTabLayout) view.findViewById(R.id.tabs);
        tabs.setCustomTabColorizer(new SlidingTabLayout.TabColorizer() {

            @Override
            public int getIndicatorColor(int position) {
                Fragment frg = fragments.get(position);
                if (frg instanceof BaseFragment) {
                    BaseFragment bfrg = (BaseFragment)frg;
                        return bfrg.getIndicatorColor();
                } else {
                    BaseListFragment blfrg = (BaseListFragment)frg;
                        return blfrg.getIndicatorColor();
                }
            }

            @Override
            public int getDividerColor(int position) {
                Fragment frg = fragments.get(position);
                if (frg instanceof BaseFragment) {
                    BaseFragment bfrg = (BaseFragment)frg;
                    return bfrg.getDividerColor();
                } else {
                    BaseListFragment blfrg = (BaseListFragment)frg;
                    return blfrg.getDividerColor();
                }
                //return fragments.get(position).getDividerColor();
            }
        });
        tabs.setBackgroundResource(R.color.color_primary);
        tabs.setCustomTabView(R.layout.tab_title, R.id.txtTabTitle, R.id.imgTabIcon);
        tabs.setViewPager(pager);

    }

    private LinkedList<Fragment> getFragments() {
        int indicatorColor = Color.parseColor(this.getResources().getString(md_blue_500));
        int dividerColor = Color.TRANSPARENT;

        LinkedList<Fragment> fragments = new LinkedList<Fragment>();
        fragments.add(ProfFragment.newInstance("Prof", indicatorColor, dividerColor, R.drawable.google));
        fragments.add(GoogleFragment.newInstance("Google", indicatorColor, dividerColor, R.drawable.google));
        fragments.add(BingFragment.newInstance("Bing", indicatorColor, dividerColor, R.drawable.bing));
        fragments.add(YahooFragment.newInstance("Yahoo", indicatorColor, dividerColor, R.drawable.yahoo));


        return fragments;
    }
}