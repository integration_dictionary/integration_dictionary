package com.example.user.myapplication;

import android.app.ListFragment;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by 世洋
 */

public class ProfFragment extends BaseListFragment {
    private static final String DATA_NAME = "name";

    private String title = "";
    private String[] cacheContext = null;
    private View root;
    private ListView listView;
    private ArrayAdapter<String> adapter;
    private Context context;
    private FragmentManager manager;
    private FragmentTransaction transaction;
    String[] list = {"資料讀取中",""};
    private static int[] scores, types;
    private boolean[] used;

    public static ProfFragment newInstance(String title, int indicatorColor, int dividerColor, int iconResId) {

        ProfFragment f = new ProfFragment();
        f.setTitle(title);
        f.setIndicatorColor(indicatorColor);
        f.setDividerColor(dividerColor);
        f.setIconResId(iconResId);

        //pass data
        Bundle args = new Bundle();
        args.putString(DATA_NAME, title);
        f.setArguments(args);

        return f;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        title = getArguments().getString(DATA_NAME);
        adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1,list);
        setListAdapter(adapter);
        if (scores == null)
            scores = new int[40];
        if (types == null)
            types = new int[40];
        used = new boolean[40];
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.frg_prof, container, false);
        return root;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        listView = getListView();
        if (cacheContext != null) {
            setContent(cacheContext);
        }
    }

    @Override
    public void setTypes(int[] types) {
        this.types = types;
    }

    @Override
    public void setScores(int[] scores) {
        this.scores = scores;
    }

    @Override
    public int[] getScores() {
        return scores;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        if (position >= 0 && position < 40 && types[position] > 0 && types[position] < 40) {
            if (used[types[position]] == false) {
                scores[types[position]]++;
                used[types[position]] = true;
            }
            Toast.makeText(getActivity(), "你選擇了" + list[position], Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void setContent(Object content) {
        try {
            cacheContext = (String[]) content;
            int num = cacheContext.length, maxj = 0;
            for (int i = 0; i < num - 1; i++) {
                maxj = i;
                for (int j = i + 1; j < num; j++)
                    if (scores[types[j]] > scores[types[maxj]])
                        maxj = j;
                String tmp = cacheContext[i];
                cacheContext[i] = cacheContext[maxj];
                cacheContext[maxj] = tmp;
                int tmpi = types[i];
                types[i] = types[maxj];
                types[maxj] = tmpi;
            }

            list = cacheContext.clone();
            adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, cacheContext);
            adapter.notifyDataSetChanged();
            setListAdapter(adapter);
            setListShown(true);
        } catch (Exception e) {
            Log.e("ProfSetContent", e.toString());
        }
    }
}
