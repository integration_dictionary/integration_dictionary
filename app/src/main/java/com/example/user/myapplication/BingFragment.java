package com.example.user.myapplication;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * Created by USER on 2016/4/18.
 */
public class BingFragment extends BaseFragment {
    private static final String DATA_NAME = "name";

    private String title = "";
    private String cacheContext = null;
    private TextView textView;

    public static BingFragment newInstance(String title, int indicatorColor, int dividerColor, int iconResId) {

        BingFragment f = new BingFragment();
        f.setTitle(title);
        f.setIndicatorColor(indicatorColor);
        f.setDividerColor(dividerColor);
        f.setIconResId(iconResId);


        //pass data
        Bundle args = new Bundle();
        args.putString(DATA_NAME, title);
        f.setArguments(args);

        return f;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //get data
        title = getArguments().getString(DATA_NAME);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.frg_bing, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        textView = (TextView) view.findViewById(R.id.txt_bing);
        textView.setText(title);
        if (cacheContext != null) {
            setContent(cacheContext);
        }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    @Override
    public void onDestroy() {

        super.onDestroy();
    }


    @Override
    public void onDestroyView() {

        super.onDestroyView();
    }

    @Override
    public void setContent(Object content) {
        try {
            String str = (String) content;
            textView.setText(str);
        } catch (Exception e) {

        }
    }
}
