package com.example.user.myapplication;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.LinkedList;


public class TabFragmentPagerAdapter extends FragmentPagerAdapter {

    LinkedList<Fragment> fragments = null;

    public TabFragmentPagerAdapter(FragmentManager fm, LinkedList<Fragment> fragments) {
        super(fm);
        if (fragments == null) {
            this.fragments = new LinkedList<Fragment>();
        } else {
            this.fragments = fragments;
        }
    }

    @Override
    public Fragment getItem(int position) {
        return fragments.get(position);
    }

    @Override
    public int getCount() {
        return fragments.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        Fragment frg = fragments.get(position);
        if (frg instanceof BaseFragment) {
            BaseFragment bfrg = (BaseFragment)frg;
            return bfrg.getTitle();
        } else {
            BaseListFragment blfrg = (BaseListFragment)frg;
            return blfrg.getTitle();
        }
    //return fragments.get(position).getTitle();
    }

    public int getIconResId(int position) {
        Fragment frg = fragments.get(position);
        if (frg instanceof BaseFragment) {
            BaseFragment bfrg = (BaseFragment)frg;
            return bfrg.getIconResId();
        } else {
            BaseListFragment blfrg = (BaseListFragment)frg;
            return blfrg.getIconResId();
        }
        //return fragments.get(position).getIconResId();
    }

}