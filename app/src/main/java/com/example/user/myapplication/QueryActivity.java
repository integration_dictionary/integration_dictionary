package com.example.user.myapplication;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.memetix.mst.language.Language;
import com.memetix.mst.translate.Translate;

public class QueryActivity extends AppCompatActivity {
    String input = null;
    String bing_output = null;
    private CleanableEditText cleanableEditText;
    private Spinner spinner1;
    private Spinner spinner2;
    private TextView previewView;
    private PreviewAsyncTask previewTask = new PreviewAsyncTask();
    private String queryStr = "";
    private MyApplication myApp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_query);

        initDeditText();
        previewView = (TextView) findViewById(R.id.preview_textView);
        myApp = (MyApplication) getApplication();
        spinner1 = (Spinner) findViewById(R.id.src_lan);
        spinner2 = (Spinner) findViewById(R.id.des_lan);


        Button bt = (Button) findViewById(R.id.button);
        bt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                input = cleanableEditText.getText().toString();
                Toast.makeText(QueryActivity.this, input, Toast.LENGTH_SHORT).show();

                Intent intent = new Intent();
                intent.setClass(QueryActivity.this, ResultActivity.class);
                intent.putExtra("input", input);
                startActivity(intent);


            }
        });

        //initialize spinner1
        spinner1.setAdapter(new ArrayAdapter<>(this, R.layout.myspinner, myApp.LanguageList));
        spinner1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
                myApp.setSrcLang(adapterView.getSelectedItemPosition());
                triggerPreviewTask(queryStr);
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });

        //initialize spinner2
        spinner2.setAdapter(new ArrayAdapter<>(this, R.layout.myspinner, myApp.LanguageList));
        spinner2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
                myApp.setDesLang(adapterView.getSelectedItemPosition());
                triggerPreviewTask(queryStr);
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });


    }

    @Override
    protected void onResume() {
        super.onResume();
        cleanableEditText.setText(myApp.DEditText);
        cleanableEditText.requestFocus();
        cleanableEditText.postDelayed(new Runnable() {
            @Override
            public void run() {
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.showSoftInput(cleanableEditText, 0);
            }
        }, 200);
        Log.d("Query/spinner1/get",new Integer(myApp.getSrcLang()).toString());
        Log.d("Query/spinner2/get",new Integer(myApp.getDesLang()).toString());
        spinner1.setSelection(myApp.getSrcLang());
        spinner2.setSelection(myApp.getDesLang());
    }

    @Override
    protected void onPause() {
        super.onPause();
        myApp.DEditText = cleanableEditText.getText().toString();
        previewTask.cancel(true);
    }

    private void initDeditText() {
        cleanableEditText = (CleanableEditText) findViewById(R.id.D_editText);

        cleanableEditText.requestFocus();
        cleanableEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                triggerPreviewTask(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private void triggerPreviewTask(String s) {
        if (previewTask.getStatus() == AsyncTask.Status.FINISHED) {
            previewTask = new PreviewAsyncTask();
            previewView.setText(previewView.getText() + "...");
        }
        if (previewTask.getStatus() == AsyncTask.Status.PENDING) {
            queryStr = s;
            previewTask.execute(queryStr);
            previewView.setText(previewView.getText() + "...");
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    class PreviewAsyncTask extends AsyncTask<String, Integer, Boolean> {
        @Override
        protected Boolean doInBackground(String... args) {
            Translate.setClientId("android_bing_translator");
            Translate.setClientSecret("fGwiwzUAOKsXC5h3zejnEKIespS8LDVNJnjLZkl9q1o=");

            try {
                bing_output = Translate.execute(args[0],myApp.bing_src_lan, myApp.bing_des_lan);
            } catch (Exception e) {
                Log.d("PreviewAsyncTask", e.toString());
            }

            return true;
        }


        protected void onPostExecute(Boolean result) {
            previewView.setText(bing_output);
            if (!cleanableEditText.getText().toString().equals(queryStr)) {
                queryStr = cleanableEditText.getText().toString();
                previewView.setText(previewView.getText() + "...");
                previewTask = new PreviewAsyncTask();
                previewTask.execute(queryStr);

            }
        }
    }

}
