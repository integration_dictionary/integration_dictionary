package com.example.user.myapplication;

import android.app.Application;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import android.widget.Adapter;
import android.widget.ArrayAdapter;

import com.memetix.mst.language.Language;

import java.util.ArrayList;
import java.util.List;

public class MyApplication extends Application {
    public String DEditText = "";
    private List<String> history_list = new ArrayList<>();
    private List<String[]> history_list_detail = new ArrayList<>();

    public List<String> getHistory() {
        return history_list;
    }

    public String[] LanguageList = {"英文", "中文", "日文"};
    private int srcLang = 0;
    private int desLang = 1;
    public Language bing_src_lan = null;
    public Language bing_des_lan = null;
    public String google_src_lan = null;
    public String google_des_lan = null;

    public int getSrcLang(){
        return srcLang;
    }
    public void setSrcLang(int srcLang){
        this.srcLang = srcLang;
        switch(srcLang) {
            case 0:
                bing_src_lan = Language.ENGLISH;
                google_src_lan = "en";
                break;
            case 1:
                bing_src_lan = Language.CHINESE_TRADITIONAL;
                google_src_lan = "zh-TW";
                break;
            case 2:
                bing_src_lan = Language.JAPANESE;
                google_src_lan = "ja";
                break;
        }
    }

    public int getDesLang(){
        return desLang;
    }
    public void setDesLang(int desLang){
        this.desLang = desLang;
        switch(desLang) {
            case 0:
                bing_des_lan = Language.ENGLISH;
                google_des_lan = "en";
                break;
            case 1:
                bing_des_lan = Language.CHINESE_TRADITIONAL;
                google_des_lan = "ZH_TW";
                break;
            case 2:
                bing_des_lan = Language.JAPANESE;
                google_des_lan = "ja";
                break;
        }
    }

    public void addHistoryEntry(Context context, String queryStr, String timestamp, String srcLang, String desLang, String result){
        SQLiteDatabase db = new MyDbHelper(context).getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(MyDbHelper.HistoryQueryEntry.COLUMN_NAME_QUERY_STRING,queryStr);
        values.put(MyDbHelper.HistoryQueryEntry.COLUMN_NAME_ENTRY_TIMESTAMP,timestamp);
        values.put(MyDbHelper.HistoryQueryEntry.COLUMN_NAME_SRC_LANG,srcLang);
        values.put(MyDbHelper.HistoryQueryEntry.COLUMN_NAME_DES_LANG,desLang);
        values.put(MyDbHelper.HistoryQueryEntry.COLUMN_NAME_RESULT,result);

        db.insertWithOnConflict(MyDbHelper.HistoryQueryEntry.TABLE_NAME, null, values,SQLiteDatabase.CONFLICT_REPLACE);
    }
    public void deleteHistoryEntry(Context context, String key){
        SQLiteDatabase db = new MyDbHelper(context).getWritableDatabase();
        db.delete(MyDbHelper.HistoryQueryEntry.TABLE_NAME, MyDbHelper.HistoryQueryEntry.COLUMN_NAME_QUERY_STRING + "='" + key + "'",null);
    }

    public void getHistoryEntries(Context context){
        SQLiteDatabase db = new MyDbHelper(context).getWritableDatabase();
        String[] projection = {
                MyDbHelper.HistoryQueryEntry.COLUMN_NAME_QUERY_STRING,
                MyDbHelper.HistoryQueryEntry.COLUMN_NAME_SRC_LANG,
                MyDbHelper.HistoryQueryEntry.COLUMN_NAME_DES_LANG,
                MyDbHelper.HistoryQueryEntry.COLUMN_NAME_RESULT
        };
        String sortOrder = MyDbHelper.HistoryQueryEntry.COLUMN_NAME_ENTRY_TIMESTAMP + " DESC";

        Cursor c = db.query(
                MyDbHelper.HistoryQueryEntry.TABLE_NAME,  // The table to query
                projection,                               // The columns to return
                null,                                // The columns for the WHERE clause
                null,                            // The values for the WHERE clause
                null,                                     // don't group the rows
                null,                                     // don't filter by row groups
                sortOrder                                 // The sort order
        );
        Log.d("getHistory","ENTER");
        history_list.clear();
        history_list_detail.clear();
        while(c.moveToNext()){
            history_list.add(c.getString(0) + "      " + c.getString(3));
            String[] detail = new String[3];
            detail[0] = c.getString(0);
            detail[1] = c.getString(1);
            detail[2] = c.getString(2);
            history_list_detail.add(detail);
            Log.d("getHistory",c.getString(0) + "      " + c.getString(1));
        }
        c.close();
    }

    public String[] useHistory(int position){
        return history_list_detail.get(position);
    }

}
