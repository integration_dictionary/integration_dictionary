package com.example.user.myapplication;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

public class YahooFragment extends BaseFragment {
    private static final String DATA_NAME = "name";

    private String title = "";
    private String cacheContext = null;
    private WebView webView;

    public static YahooFragment newInstance(String title, int indicatorColor, int dividerColor, int iconResId) {

        YahooFragment f = new YahooFragment();
        f.setTitle(title);
        f.setIndicatorColor(indicatorColor);
        f.setDividerColor(dividerColor);
        f.setIconResId(iconResId);


        //pass data
        Bundle args = new Bundle();
        args.putString(DATA_NAME, title);
        f.setArguments(args);

        return f;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //get data
        title = getArguments().getString(DATA_NAME);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.frg_yahoo, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        webView = (WebView) view.findViewById(R.id.web_yahoo);
        if (cacheContext != null) {
            setContent(cacheContext);
        }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    @Override
    public void onDestroy() {

        super.onDestroy();
    }


    @Override
    public void onDestroyView() {

        super.onDestroyView();
    }


    @Override
    public void setContent(Object content) {
        try {
            cacheContext = (String) content;
            Log.d("YahooSetContent", cacheContext);
            if (webView != null) {
                webView.loadDataWithBaseURL("http://tw.dictionary.search.yahoo.com/", cacheContext, "text/html", "utf-8", null);
            }
        } catch (Exception e) {

        }
    }
}
