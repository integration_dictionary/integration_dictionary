package com.example.user.myapplication;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;
import android.util.Log;

public class MyDbHelper extends SQLiteOpenHelper{
    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "MyDB.db";
    private static final String TEXT_TYPE = " TEXT";
    private static final String COMMA_SEP = ",";
    private static final String SQL_CREATE_ENTRIES =
            "CREATE TABLE " + HistoryQueryEntry.TABLE_NAME + " (" +
                    HistoryQueryEntry.COLUMN_NAME_QUERY_STRING + TEXT_TYPE + " PRIMARY KEY," +
                    HistoryQueryEntry.COLUMN_NAME_ENTRY_TIMESTAMP + TEXT_TYPE + COMMA_SEP +
                    HistoryQueryEntry.COLUMN_NAME_SRC_LANG + TEXT_TYPE + COMMA_SEP +
                    HistoryQueryEntry.COLUMN_NAME_DES_LANG + TEXT_TYPE + COMMA_SEP +
                    HistoryQueryEntry.COLUMN_NAME_RESULT + TEXT_TYPE +
            " )";

    private static final String SQL_DELETE_ENTRIES =
            "DROP TABLE IF EXISTS " + HistoryQueryEntry.TABLE_NAME;

    public MyDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.d("createTable",SQL_CREATE_ENTRIES);
        db.execSQL(SQL_CREATE_ENTRIES);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(SQL_DELETE_ENTRIES);
        onCreate(db);
    }

    public static abstract class HistoryQueryEntry implements BaseColumns {
        public static final String TABLE_NAME = "history";
        public static final String COLUMN_NAME_QUERY_STRING = "queryStr";
        public static final String COLUMN_NAME_ENTRY_TIMESTAMP = "timestamp";
        public static final String COLUMN_NAME_SRC_LANG = "srcLang";
        public static final String COLUMN_NAME_DES_LANG = "desLang";
        public static final String COLUMN_NAME_RESULT = "result";
    }

}

