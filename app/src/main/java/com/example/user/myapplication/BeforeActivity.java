package com.example.user.myapplication;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.memetix.mst.language.Language;

public class BeforeActivity extends AppCompatActivity {
    String input = null;
    private CleanableEditText cleanableEditText;
    private Spinner spinner1;
    private Spinner spinner2;
    private ListView list;
    private MyApplication myApp;
    private ArrayAdapter<String> record ;
    private Button bt;


    private Toolbar.OnMenuItemClickListener onMenuItemClick = new Toolbar.OnMenuItemClickListener() {
        @Override
        public boolean onMenuItemClick(MenuItem menuItem) {
            String msg = "";
            switch (menuItem.getItemId()) {
                case R.id.setting1:
                    msg += "我的最愛";
                    break;
            }
            if (!msg.equals("")) {
                Toast.makeText(BeforeActivity.this, msg, Toast.LENGTH_SHORT).show();
            }
            return true;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_before);
        initToolbar();
        initDeditText();

        list = (ListView) findViewById(R.id.listView);
        myApp = (MyApplication) getApplication();
        spinner1 = (Spinner) findViewById(R.id.src_lan);
        spinner2 = (Spinner) findViewById(R.id.des_lan);

        bt = (Button) findViewById(R.id.button);

        record = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, myApp.getHistory());
        list.setAdapter(record);

        /*list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {

            }

        });*/
        bt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               // onDEditTextClick(v);

                input = cleanableEditText.getText().toString();
                Toast.makeText(BeforeActivity.this, input, Toast.LENGTH_SHORT).show();

                Intent intent = new Intent();
                intent.setClass(BeforeActivity.this, ResultActivity.class);
                intent.putExtra("input", input);
                startActivity(intent);
            }
        });

        list.setOnTouchListener(new View.OnTouchListener(){
            float x, y, upx, upy;
            public boolean onTouch(View view, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    x = event.getX();
                    y = event.getY();
                }
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    upx = event.getX();
                    upy = event.getY();
                    int position1 = ((ListView) view).pointToPosition((int) x, (int) y);
                    int position2 = ((ListView) view).pointToPosition((int) upx,(int) upy);

                    if(position1 == position2 && Math.abs(x-upx) <=5){
                        String[] detail = myApp.useHistory(position1);
                        String input = detail[0];
                        Intent intent = new Intent();
                        intent.setClass(BeforeActivity.this, ResultActivity.class);
                        intent.putExtra("input", input);
                        startActivity(intent);

                    }
                    else if (position1 == position2 && (x - upx) < -5) {
                        View v = ((ListView) view).getChildAt(position1);
                        removeListItem(v,position1,1);
                    }
                    else if (position1 == position2 && (x - upx) > 5) {
                        View v = ((ListView) view).getChildAt(position1);
                        removeListItem(v,position1,2);
                    }
                }
                return false;
            }

                                });

                //initialize spinner1
        spinner1.setAdapter(new ArrayAdapter<>(this, R.layout.myspinner, myApp.LanguageList));
        spinner1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
                myApp.setSrcLang(adapterView.getSelectedItemPosition());
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });

        //initialize spinner2
        spinner2.setAdapter(new ArrayAdapter<>(this, R.layout.myspinner, myApp.LanguageList));
        spinner2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
                myApp.setDesLang(adapterView.getSelectedItemPosition());
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });

    }

    protected void removeListItem(View rowView, final int positon, int flag) {

        if(flag==1) {
            final Animation animation = (Animation) AnimationUtils.loadAnimation(rowView.getContext(), R.anim.animation);
            animation.setAnimationListener(new Animation.AnimationListener() {
                public void onAnimationStart(Animation animation) {
                }

                public void onAnimationRepeat(Animation animation) {
                }

                public void onAnimationEnd(Animation animation) {

                    String[] detail = myApp.useHistory(positon);
                    myApp.deleteHistoryEntry(getApplicationContext(), detail[0]);
                    myApp.getHistoryEntries(BeforeActivity.this);
                    ((ArrayAdapter) list.getAdapter()).notifyDataSetChanged();
                    record.notifyDataSetChanged();

                    animation.cancel();
                }
            });
            rowView.startAnimation(animation);
        }
        else if(flag==2){
            final Animation animation2 = (Animation) AnimationUtils.loadAnimation(rowView.getContext(), R.anim.animation2);
            animation2.setAnimationListener(new Animation.AnimationListener() {
                public void onAnimationStart(Animation animation2) {
                }

                public void onAnimationRepeat(Animation animation2) {
                }

                public void onAnimationEnd(Animation animation2) {

                    String[] detail = myApp.useHistory(positon);
                    myApp.deleteHistoryEntry(getApplicationContext(), detail[0]);
                    myApp.getHistoryEntries(BeforeActivity.this);
                    ((ArrayAdapter) list.getAdapter()).notifyDataSetChanged();
                    record.notifyDataSetChanged();

                    animation2.cancel();
                }
            });
            rowView.startAnimation(animation2);
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        cleanableEditText.setText(myApp.DEditText);
   //     ((ArrayAdapter) list.getAdapter()).notifyDataSetChanged();
   //     record.notifyDataSetChanged();
        spinner1.setSelection(myApp.getSrcLang());
        spinner2.setSelection(myApp.getDesLang());
        myApp.getHistoryEntries(this);
        ((ArrayAdapter) list.getAdapter()).notifyDataSetChanged();
        record.notifyDataSetChanged();
    }

    @Override
    protected void onPause() {
        super.onPause();
        myApp.DEditText = cleanableEditText.getText().toString();
    }

    private void initToolbar() {
        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView mToolBarTextView = (TextView) findViewById(R.id.text_view_toolbar_title);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setHomeButtonEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        mToolbar.setOnMenuItemClickListener(onMenuItemClick);

    }

    private void initDeditText() {
        cleanableEditText = (CleanableEditText) findViewById(R.id.D_editText);
    }

    public void onDEditTextClick(View view) {
        Intent intent = new Intent(this, QueryActivity.class);
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

}
