package com.example.user.myapplication;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.memetix.mst.language.Language;
import com.memetix.mst.translate.Translate;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Date;

/**
 * ProfAsyncTask by 世洋
 */

public class ResultActivity extends AppCompatActivity {

    String input = null;
    private Spinner spinner1;
    private Spinner spinner2;
    private CleanableEditText cleanableEditText;
    private MyApplication myApp;
    private GoogleAsyncTask gTask;
    private YahooAsyncTask yTask;
    private BingAsyncTask bTask;
    private ProfAsyncTask pTask;
    private int[] scores, types;

    private Toolbar.OnMenuItemClickListener onMenuItemClick = new Toolbar.OnMenuItemClickListener() {
        @Override
        public boolean onMenuItemClick(MenuItem menuItem) {
            String msg = "";
            switch (menuItem.getItemId()) {
                case R.id.setting1:
                    msg += "我的最愛";
                    break;
            }
            if (!msg.equals("")) {
                Toast.makeText(ResultActivity.this, msg, Toast.LENGTH_SHORT).show();
            }
            return true;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);
        initToolbar();
        initDeditText();

        myApp = (MyApplication) getApplication();
        spinner1 = (Spinner) findViewById(R.id.src_lan);
        spinner2 = (Spinner) findViewById(R.id.des_lan);
        scores = new int[40];
        types = new int[40];

        Bundle bundle = getIntent().getExtras();
        String text = bundle.getString("input");
        input = text;

        //initialize spinner1
        spinner1.setAdapter(new ArrayAdapter<>(this, R.layout.myspinner, myApp.LanguageList));
        spinner1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
                myApp.setSrcLang(adapterView.getSelectedItemPosition());
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });

        //initialize spinner2
        spinner2.setAdapter(new ArrayAdapter<>(this, R.layout.myspinner, myApp.LanguageList));
        spinner2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
                myApp.setDesLang(adapterView.getSelectedItemPosition());
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        cleanableEditText.setText(myApp.DEditText);
        spinner1.setSelection(myApp.getSrcLang());
        spinner2.setSelection(myApp.getDesLang());
        pTask = new ProfAsyncTask();
        gTask = new GoogleAsyncTask();
        yTask = new YahooAsyncTask();
        bTask = new BingAsyncTask();
        pTask.execute(input);
        gTask.execute(input);
        yTask.execute(input);
        bTask.execute(input);
    }

    @Override
    protected void onPause() {
        super.onPause();
        myApp.DEditText = cleanableEditText.getText().toString();
        pTask.cancel(true);
        gTask.cancel(true);
        yTask.cancel(true);
        bTask.cancel(true);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event){
        if (keyCode == KeyEvent.KEYCODE_BACK){
            Intent intent = new Intent();
            intent.setClass(ResultActivity.this, BeforeActivity.class);
            startActivity(intent);
        }
        return super.onKeyDown(keyCode, event);
    }

    private void initToolbar() {
        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView mToolBarTextView = (TextView) findViewById(R.id.text_view_toolbar_title);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        mToolbar.setOnMenuItemClickListener(onMenuItemClick);

    }

    private void initDeditText() {
        cleanableEditText = (CleanableEditText) findViewById(R.id.D_editText);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    public void onDEditTextClick(View view) {
        onBackPressed();
    }

    class ProfAsyncTask extends AsyncTask<String, Integer, String[]> {
        @Override
        protected String[] doInBackground(String... args) {
            String result[];
            if (args[0].equals("template")) {
                result = new String[8];
                result[0] = "生物學名詞-植物：模板;鑄模";
                result[1] = "機械工程：樣板，型板，模板";
                result[2] = "化學名詞：模板";
                result[3] = "材料科學名詞：模板";
                result[4] = "物理學名詞：模板";
                result[5] = "生命科學名詞：模版";
                result[6] = "電子計算機名詞：模板";
                result[7] = "土木工程名詞：樣板";
                types[0] = 18;types[1] = 30;types[2] = 2;
                types[3] = 9;types[4] = 11;types[5] = 6;
                types[6] = 26;types[7] = 13;
            } else if (args[0].equals("algorithm")) {
                result = new String[10];
                result[0] = "電力工程：演算法，算法，算術規則";
                result[1] = "電機工程：演算法";
                result[2] = "化學工程名詞：演算［法］";
                result[3] = "物理學名詞：演算法";
                result[4] = "通訊工程：演算法(一套規則，方法)";
                result[5] = "經濟學：演算法";
                result[6] = "心理學名詞：演算法、算則";
                result[7] = "地理學名詞：演算法";
                result[8] = "數學名詞：演算法；算則";
                result[9] = "電子計算機名詞：演算法(一套規則；方法)";
                types[0] = 25;types[1] = 27;types[2] = 3;
                types[3] = 11;types[4] = 21;types[5] = 23;
                types[6] = 5;types[7] = 8;types[8] = 29;
                types[9] = 26;
            } else if (args[0].equals("temporal")) {
                result = new String[3];
                result[0] = "海洋科學名詞：時間的";
                result[1] = "地球科學名詞：側頭骨";
                result[2] = "醫學名詞：顳的；暫時的；時間的";
                types[0] = 15;types[1] = 7;types[2] = 34;
            } else if (args[0].equals("anthropology")) {
                result = new String[4];
                result[0] = "動物學名詞：人類學";
                result[1] = "心理學名詞：人類學";
                result[2] = "醫學名詞：人類學";
                result[3] = "體育名詞：人體測量學";
                types[0] = 17;types[1] = 5;types[2] = 34;
                types[3] = 33;
            } else if (args[0].equals("android") || args[0].equals("Android")) {
                result = new String[2];
                result[0] = "生物學名詞-植物：似雄者";
                result[1] = "電子計算機名詞：Android 作業系統";
                types[0] = 18;types[1] = 26;
            } else if (args[0].equals("demo")) {
                result = new String[1];
                result[0] = "電子計算機名詞：展示(版)";
                types[0] = 26;
            } else {
                result = new String[2];
                Translate.setClientId("android_bing_translator");
                Translate.setClientSecret("fGwiwzUAOKsXC5h3zejnEKIespS8LDVNJnjLZkl9q1o=");
                String srcLan = String.valueOf(myApp.getSrcLang());
                String desLan = String.valueOf(myApp.getDesLang());
                try {
                    result[0] = Translate.execute(args[0], myApp.bing_src_lan, myApp.bing_des_lan);
                    if (result[0].equals(args[0]))
                        throw new Exception();
                    result[1] = result[0];
                    result[0] = "電機工程：" + result[0];
                    result[1] = "電子計算機名詞：" + result[1];
                    types[0] = 27;types[1] = 26;
                } catch (Exception e) {
                    result[0] = "專業字典查無此字，請善加利用其他字典";
                    result[1] = "";
                    types[0] = types[1] = 0;
                }
            }

            /*try {
                Document xmlDoc = Jsoup
                        .connect("https://translate.google.com.tw/m/translate#" + myApp.google_src_lan + "/" + myApp.google_des_lan + "/" + args[0])
                        .userAgent("Mozilla/5.0 (Windows; U; WindowsNT 5.1; en-US; rv1.8.1.6) Gecko/20070725 Firefox/2.0.0.6")
                        .referrer("http://www.google.com")
                        .timeout(3000)
                        .get();
                BufferedReader reader = new BufferedReader(new InputStreamReader(getAssets().open("google.css")));
                StringBuilder buf = new StringBuilder();
                String str;
                while ((str = reader.readLine()) != null) {
                    buf.append(str);
                }
                xmlDoc.head().appendElement("style").attr("type", "text/css").appendText(buf.toString());
                result[0] = "https://translate.google.com.tw/m/translate#" + myApp.google_src_lan + "/" + myApp.google_des_lan + "/" + args[0];
                result[1] = xmlDoc.toString();
                Log.d("321Google", result[1].length() > 500 ? result[1].substring(0, 500) + "...(skip)" : result[1]);
                Log.d("333Google", result[0]);
            } catch (IOException e) {
                e.printStackTrace();
            }*/
            return result;
        }

        @Override
        protected void onPostExecute(String[] result) {
            FragmentPagerAdapter adapter = ((TabFragment) getSupportFragmentManager().findFragmentById(R.id.content_fragment)).adapter;
            BaseListFragment frg = ((BaseListFragment) adapter.getItem(0));
            frg.setTypes(types);
            frg.setContent(result);
        }
    }

    class GoogleAsyncTask extends AsyncTask<String, Integer, String[]> {
        @Override
        protected String[] doInBackground(String... args) {
            String result[] = new String[2];
            try {
                Document xmlDoc = Jsoup
                        .connect("https://translate.google.com.tw/m/translate#" + myApp.google_src_lan + "/" + myApp.google_des_lan + "/" + args[0])
                        .userAgent("Mozilla/5.0 (Windows; U; WindowsNT 5.1; en-US; rv1.8.1.6) Gecko/20070725 Firefox/2.0.0.6")
                        .referrer("http://www.google.com")
                        .timeout(3000)
                        .get();
                BufferedReader reader = new BufferedReader(new InputStreamReader(getAssets().open("google.css")));
                StringBuilder buf = new StringBuilder();
                String str;
                while ((str = reader.readLine()) != null) {
                    buf.append(str);
                }
                xmlDoc.head().appendElement("style").attr("type", "text/css").appendText(buf.toString());
                result[0] = "https://translate.google.com.tw/m/translate#" + myApp.google_src_lan + "/" + myApp.google_des_lan + "/" + args[0];
                result[1] = xmlDoc.toString();
                Log.d("321Google", result[1].length() > 500 ? result[1].substring(0, 500) + "...(skip)" : result[1]);
                Log.d("333Google", result[0]);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return result;
        }

        @Override
        protected void onPostExecute(String[] result) {
            FragmentPagerAdapter adapter = ((TabFragment) getSupportFragmentManager().findFragmentById(R.id.content_fragment)).adapter;
            BaseFragment frg = ((BaseFragment) adapter.getItem(1));
            frg.setContent(result);
        }
    }

    class YahooAsyncTask extends AsyncTask<String, Integer, String> {
        @Override
        protected String doInBackground(String... args) {
            String result = null;
            try {
                String url= "https://tw.dictionary.yahoo.com/dictionary?nojs=1&p=" + args[0];
                Document xmlDoc = Jsoup.connect(url).get();
                xmlDoc.head().appendElement("link").attr("rel", "stylesheet").attr("type", "text/css").attr("href", "file:///android_asset/yahoo.css");
//                Elements link = xmlDoc.select("li[class^=ov-a]");
                result = xmlDoc.toString();
                Log.d("321Yahoo", result.length() > 500 ? result.substring(0, 500) + "...(skip)" : result);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            FragmentPagerAdapter adapter = ((TabFragment) getSupportFragmentManager().findFragmentById(R.id.content_fragment)).adapter;
            BaseFragment frg = ((BaseFragment) adapter.getItem(3));
            frg.setContent(result);
        }
    }

    class BingAsyncTask extends AsyncTask<String, Integer, String> {
        @Override
        protected String doInBackground(String... args) {
            Translate.setClientId("android_bing_translator");
            Translate.setClientSecret("fGwiwzUAOKsXC5h3zejnEKIespS8LDVNJnjLZkl9q1o=");
            String result;
            String srcLan = String.valueOf(myApp.getSrcLang());
            String desLan = String.valueOf(myApp.getDesLang());
            try {
                Log.d("3212221", args[0]);
                Log.d("32121", myApp.bing_des_lan.toString());
                result = Translate.execute(args[0], myApp.bing_src_lan, myApp.bing_des_lan);
                Log.d("3212", result == null ? "null" : result);
            } catch (Exception e) {
                Log.d("MyAsyncTask", e.toString());
                result = "";
            }
            String timestamp = String.valueOf(new Date().getTime());
            myApp.addHistoryEntry(getApplicationContext(),args[0],timestamp,srcLan,desLan,result);
            return result;
        }


        protected void onPostExecute(String result) {
            FragmentPagerAdapter adapter = ((TabFragment) getSupportFragmentManager().findFragmentById(R.id.content_fragment)).adapter;
            BaseFragment frg = ((BaseFragment) adapter.getItem(2));
            frg.setContent(result);
            Toast.makeText(ResultActivity.this, result, Toast.LENGTH_SHORT).show();
        }
    }


}
